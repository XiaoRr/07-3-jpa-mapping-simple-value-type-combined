package com.twuc.webApp.domain.composite;

import javax.persistence.*;

@Entity(name = "user_profile")
public class UserProfile {
    @Column
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false, length = 128, name = "address_city")
    private
    String addressCity;

    @Column(nullable = false, length = 128, name = "address_street")
    private
    String addressStreet;

    public long getId() {
        return id;
    }

    public String getAddressCity() {
        return addressCity;
    }

    public String getAddressStreet() {
        return addressStreet;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setAddressCity(String addressCity) {
        this.addressCity = addressCity;
    }

    public void setAddressStreet(String addressStreet) {
        this.addressStreet = addressStreet;
    }

    public UserProfile() {
    }

    public UserProfile(String addressCity, String addressStreet) {
        this.addressCity = addressCity;
        this.addressStreet = addressStreet;
    }
}
