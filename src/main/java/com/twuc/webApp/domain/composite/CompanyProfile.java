package com.twuc.webApp.domain.composite;

import javax.persistence.*;

@Entity(name = "company_profile")
public class CompanyProfile {
    @Column
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false, length = 128)
    private
    String city;

    @Column(nullable = false, length = 128)
    private
    String street;

    public long getId() {
        return id;
    }

    public String getCity() {
        return city;
    }

    public String getStreet() {
        return street;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public CompanyProfile() {
    }

    public CompanyProfile(String city, String street) {
        this.city = city;
        this.street = street;
    }
}
