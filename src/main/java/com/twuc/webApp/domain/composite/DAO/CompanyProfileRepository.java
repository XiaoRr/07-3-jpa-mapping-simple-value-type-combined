package com.twuc.webApp.domain.composite.DAO;

import com.twuc.webApp.domain.composite.CompanyProfile;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CompanyProfileRepository extends JpaRepository<CompanyProfile, Long> {
}
