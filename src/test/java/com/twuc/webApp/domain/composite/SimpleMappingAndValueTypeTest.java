package com.twuc.webApp.domain.composite;

import com.twuc.webApp.domain.composite.DAO.CompanyProfileRepository;
import com.twuc.webApp.domain.composite.DAO.UserProfileRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;

import javax.persistence.EntityManager;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest(showSql = false)
//@DirtiesContext (methodMode = MethodMode.BEFORE_METHOD)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
public class SimpleMappingAndValueTypeTest {
    @Autowired
    private UserProfileRepository userProfileRepo;
    @Autowired
    private CompanyProfileRepository companyProfileRepo;
    @Autowired
    private EntityManager em;
    @Test
    void test_company_profile() {
        CompanyProfile profile = companyProfileRepo.save(new CompanyProfile("Xi'an", "Gao Xin Street"));
        companyProfileRepo.flush();
        em.clear();
        CompanyProfile savedProfile = companyProfileRepo.getOne(profile.getId());
        companyProfileRepo.flush();
        assertEquals("Xi'an",savedProfile.getCity());
    }
    @Test
    void test_user_profile() {
        UserProfile profile = userProfileRepo.save(new UserProfile("Xi'an", "Gao Xin Street"));
        userProfileRepo.flush();
        em.clear();
        UserProfile savedProfile = userProfileRepo.getOne(profile.getId());
        userProfileRepo.flush();
        assertEquals("Xi'an",savedProfile.getAddressCity());
    }
}
